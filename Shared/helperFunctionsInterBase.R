cat("Laden van script helperFunctionsInterBase.R \n")
# ! - **helperFunctionsInterBase.R** - this script contains the following functions:

# Verkrijgen van relevante libraries en functies
source('./Shared/helperFunctionsSettings.R') 

# Welke libraries zijn nodig in de diverse scripts?
installLoadPackages(c("dplyr", "DBI", "odbc"))

# Functie om connectie naar lokale InterBase DB opzetten
# !     - getDatabaseConnection: function to open a connection to the InterBase database. Warnings and errors are given if something goes wrong during connect
getDatabaseConnection <- function(){
  
  # Proberen connectie te maken met database
  output = tryCatch({
    
    dbConnect(odbc::odbc(),
              driver = appsettings$InterBase$driver,
              server = appsettings$InterBase$server,
              database = appsettings$InterBase$database,
              uid = appsettings$InterBase$username,
              pwd = appsettings$InterBase$password)
  },
  error = function(cond){
    message(cond)
    stop()
  },
  warning = function(cond){
    message(cond)
  })
  
  if(length(dbListTables(output))<1){
    warning("Geen tabellen gevonden in InterBase database, return NA!")
    output = FALSE
  }
  
  return(output)
}

# Functie om data uit een InterBase tabel te halen met een SQL opdracht
# !     - executeSQL: function to execute a SELECT-statement in the InterBase database and returning a dataframe with the data read
executeSQL <- function(sqlOpdracht){
  
  # Verkrijgen van database connectie
  dbConnect = getDatabaseConnection()
  output = NA
  
  output = dbGetQuery(dbConnect, sqlOpdracht)
  
  if(nrow(output) < 1){
    warning("Selectie bevat geen records!")
  }
  
  # Sluiten connectie met database
  dbDisconnect(dbConnect)
  rm(dbConnect)
  
  return(output)
}

# Functie om data uit een InterBase tabel te halen
# !     - getDatabaseTable: function to get the full content of a table in the InterBase database
getDatabaseTable <- function(tableName){
  
  # Verkrijgen van database connectie
  dbConnect = getDatabaseConnection()
  output = NA
  
  # Bestaat de tabel in de
  if(tableName %in% dbListTables(dbConnect)){
    
    output = tbl(dbConnect, tableName) %>%
      dplyr::select(!any_of(c("index"))) %>%
      dplyr::distinct() %>%
      collect()
    
    if(nrow(output) < 1){
      warning("Tabel ", tableName, " bevat geen records!")
    }
  } else {
    warning("Tabel ", tableName, " niet kunnen vinden in database! Returning NA")
  }
  
  # Sluiten connectie met database
  dbDisconnect(dbConnect)
  rm(dbConnect)
  
  return(output)
}

# !     - executeDML: function to execute a SQL-statement - INSERT, UPDATE or DELETE - in the InterBase database
executeDML <- function(sqlOpdracht){
  
  # Verkrijgen van database connectie
  dbConnect = getDatabaseConnection()
  
  dbExecute(dbConnect, sqlOpdracht)
  
  # Sluiten connectie met database
  dbDisconnect(dbConnect)
  rm(dbConnect)
}

# Functie om tabellen op te slaan in een InterBase database.
# !     - writeTableToDatabase: function to write a dataframe to a table in the InterBase database
writeTableToDatabase <- function(tableObject, tableName, doAppend = FALSE, doOverwrite = TRUE){
  
  # Wegschrijven van de data naar lokale InterBase database. TableObject mag geen class 'Date' hebben!
  if(is.data.frame(tableObject)){
    tableObject = tableObject %>% dplyr::mutate_if(is.Date, as.character)
    tableObject = tableObject %>% dplyr::mutate_if(is.POSIXct, as.character)
  }
  
  # Wegschrijven naar InterBase
  dbConnect = getDatabaseConnection()
  dbWriteTable(conn = dbConnect, name = tableName, value = tableObject, row.names = FALSE, append = doAppend, overwrite = doOverwrite)
  
  # Sluiten connectie met database
  dbDisconnect(dbConnect)
}


cat("Laden van script helperFunctionsInterBase.R VOLTOOID \n")

